[![pipeline status](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/badges/master/pipeline.svg)](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/-/pipelines?ref=master)
[![compile](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![test](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/builds/artifacts/master/raw/.results/test.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4/-/jobs/artifacts/master/file/.results/test.log?job=evaluate)

# Lab02: Matrix

> This assignment project represents 2D matrices with two different constructors and several basic operations (methods). Note: The `Matrix` class represents `Matrix` objects, therefore its methods are not static. In order to call these methods, you must call them from an object of the `Matrix` class.

- For the sake of this lab **you will be using terminal (CLI)**. Use shell compatible terminal, such as natively in Linux (Ubuntu) or in MAC OS. For Windows use [Ubuntu for Windows](https://ubuntu.com/tutorials/ubuntu-on-windows) or any other if you know what you're doing. Make sure you have `git` installed and available from the terminal.
- This assignment is completed when all tests are passing.

Before you start working on the assignment, always make sure there is **no pending merge request**! You may refer to the [AMS Tutorial for Solvers](https://docs.google.com/document/d/1UrVbwJ5e2kDnNlxiN3vVqeRovPBC4NF0TDmf4guMmSY/edit?usp=sharing) if you need.

## CLI tutorial: Create local copy (one-time)

1. Make sure you have your SSH key added in your GitLab account.
   1. Create your local SSH key if you don't have one already. Run the following command in the terminal. **Do not rewrite your existing key if it exists!** If you are prompted to rewrite an existing key, say `no`. You may leave the passphrase empty or change/remove the passphrase anytime later.
       ```sh
       ssh-keygen -f ~/.ssh/id_rsa
       ```
   1. Display the public key file and **copy its content into your clipboard**.
       ```sh
       cat ~/.ssh/id_rsa.pub
       ```
   1. Navigate to [your GitLab SSH keys settings](https://gitlab.com/-/profile/keys).
   1. Paste the public key into _Key_ field and press _Add key_ button.
1. Make sure you have your project cloned in your computer.
   1. Navigate into your course folder, e.g. `~/csc220`.
       ```sh
       mkdir ~/csc220
       cd ~/csc220
       ```
   1. Clone the assignment repository into the current folder. Type `yes` if asked whether to continue.
       ```sh
       git clone git@gitlab.com:umiami/george/csc220/fall20/lab02-solver4.git
       ```
1. Make sure your name and email are set accordingly.
   1. Verify git name and email information on your local environment by running the following commands. Use `--global` option unless you know what you're doing.
       ```sh
       git config --global user.name
       git config --global user.email
       ```
   1. If values are not set (outputs are empty) or you want to change them, add **your name and email** at the end of each command. E.g. if your name is "John Doe" and your email is "johndoe@example.com".
       ```sh
       git config --global user.name "John Doe"
       git config --global user.email "johndoe@example.com"
       ```

## CLI tutorial: Modify a file routine

1. Navigate into the cloned repository.
   ```sh
   cd ~/csc220/lab02-*
   ```
1. Always make sure your local repository is synchronized with GitLab before you start working!
   ```sh
   git pull
   ```
1. Open the source file in your favorite editor, e.g. `gedit`. Feel free to use any other (basic) editor you prefer, e.g. VS Code, Sublime, Notepad++, Atom. You can open the file from your favorite editor directly.
   ```sh
   gedit src/main/Matrix.java
   ```
1. Proceed modifications as instructed. For the first step in the lab part (the constructor), paste the following code into a proper place.
   ```java
   numRows = rowDim;
   numColumns = colDim;
   data = new int[numRows][numColumns];
   ```
1. Save changes, close the editor and double-check the current modification. For the first step in the lab part (below) you should see the three added lines. Make sure you like your modifications (including the indentation).
   ```sh
   git diff
   ```
1. Make sure your project compiles and runs executing the following commands with no errors or warnings. [Install Java JDK](https://docs.oracle.com/en/java/javase/11/install/overview-jdk-installation.html) if the following commands are missing.
   ```sh
   javac src/main/Matrix.java
   java -classpath src/main Matrix
   ```
1. Make sure all expected tests are passing. (optional)
   1. Execute the following command. [Install Maven](https://www.baeldung.com/install-maven-on-windows-linux-mac) if the following command is missing.
       ```sh
       mvn test
       ```
   1. Look for a summary line. After the first step in the lab part (below) it should look like this:
       ```sh
       [ERROR] Tests run: 7, Failures: 6, Errors: 0, Skipped: 0
       ```
1. Commit current changes with an appropriate commit message and push them back into the GitLab repository. For the first step in the lab part (below) it may look like this.
   ```sh
   git commit --all --message "Implement the constructor with dimension arguments."
   git push
   ```
1. Go to the [project's repository on GitLab](https://gitlab.com/umiami/george/csc220/fall20/lab02-solver4) and make sure the expected number of tests are passing. After the first step in the lab part (below) the test passing badge should show `1/7`.

## Instructions: Lab part

1. Implement the `Matrix` constructor with dimension arguments.
   - Follow the tutorials above.
1. Implement the `Matrix` constructor with an array argument.
   - Follow the same tutorials above with a specific implementation and a commit message.
1. Implement the `toString` method.
   - Same as above :-)

## Instructions: Assignment part

1. Implement the `equals` method following the tutorial.
1. Implement the `times` method following the tutorial.
1. Implement the `plus` method following the tutorial.
1. Implement the `transpose` method following the tutorial.
